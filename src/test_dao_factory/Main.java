package test_dao_factory;

public class Main {
	
	// Escoger "xml" o "mysql" 
	private static String implementacion = "xml";

	public static void main(String[] args) {
		System.out.println("Inicio Programa");
		
		User user1 = new User(34, "Carlos Garcia");
		UserDao user1Dao = UserDaoFactory.getDao();
		user1Dao.insert(user1);
		
		Book book1 = new Book(42, "El Quijote");
		BookDao book1Dao = BookDaoFactory.getDao();
		book1Dao.insert(book1);
		
		System.out.println("Fin Programa");
	}

	public static String getImplementacion() {
		return implementacion;
	}
}
