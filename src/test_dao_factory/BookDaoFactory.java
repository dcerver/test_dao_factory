package test_dao_factory;

public class BookDaoFactory {
	public static BookDao getDao() {
		String implementacion = Main.getImplementacion();
		if (implementacion.equals("xml")) {
			return new BookDaoXml();
		}
		else if (implementacion.equals("sql")) {
			//return new BookDaoSql();
		}
		return null;
	}
}
