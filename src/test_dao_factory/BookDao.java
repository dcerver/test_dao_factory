package test_dao_factory;

public interface BookDao {

	void insert(Book t);
	void update(Book t);
	void delete(int id);
}
