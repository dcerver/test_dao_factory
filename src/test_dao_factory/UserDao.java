package test_dao_factory;

public interface UserDao {
	
	void insert(User t);
	void update(User t);
	void delete(int id);
}
