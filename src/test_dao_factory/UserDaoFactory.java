package test_dao_factory;

public class UserDaoFactory {
	public static UserDao getDao() {
		String implementacion = Main.getImplementacion();
		if (implementacion.equals("xml")) {
			return new UserDaoXml();
		}
		else if (implementacion.equals("sql")) {
			//return new UserDaoSql();
		}
		return null;
	}
}
